import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import FutureImage from "next/future/image";
import Image from "next/image";
import Navbar from "../../components/shared/Navbar";
import { ClipLoader } from "react-spinners";
import NotFound from "../../components/shared/NotFound";

interface MovieRatingProps {
    Source: string;
    Value: string;
}

interface MovieProps {
    Actors: string;
    Awards: string;
    BoxOffice: string;
    Country: string;
    DVD: string;
    Director: string;
    Genre: string;
    Language: string;
    Metascore: string;
    Plot: string;
    Poster: string;
    Production: string;
    Rated: string;
    Ratings: MovieRatingProps[];
    Released: string;
    Response: string;
    Runtime: string;
    Title: string;
    Type: string;
    Website: string;
    Writer: string;
    Year: string;
    ImdbID: string;
    ImdbRating: string;
    ImdbVotes: string;
}

export default function Detail() {
    const router = useRouter();
    const { id } = router.query;

    const [isLiked, setIsLiked] = useState(false);

    useEffect(() => {
        if (id) {
            const storageValue = window.localStorage.getItem(id as string);
            if (storageValue) {
                const parsedValue = JSON.parse(storageValue);
                setIsLiked(Boolean(parsedValue));
            }
        }
    }, [id]);

    const {
        data: movie,
        isLoading,
        isSuccess,
    } = useQuery<MovieProps>(
        ["movies", id],
        () => fetch(`http://www.omdbapi.com/?i=${id}&apikey=2763f581`).then((res) => res.json()),
        { enabled: !!id }
    );

    const onAddToFavoritesClicked = () => {
        setIsLiked((prev) => {
            window.localStorage.setItem(id as string, String(!prev));
            return !prev;
        });
    };

    return (
        <div className="container">
            <Navbar />
            <a className="cursor-pointer" onClick={() => router.back()}>
                <Image src={"/icon-arrow-grey.svg"} alt="icon-arrow" width={22} height={22} />
            </a>
            {movie && movie.Response === "True" ? (
                <div className="detail-container">
                    <div className="detail-container--description">
                        <div className="detail--basic-info">
                            <span className="basic-info--text text--light-grey">{movie.Runtime}</span>
                            <span className="text--light-grey">&#183;</span>
                            <span className="basic-info--text text--light-grey">{movie.Year}</span>
                            <span className="text--light-grey">&#183;</span>
                            <span className="label--filled">{movie.Rated}</span>
                        </div>
                        <h1 className="detail--movie-title">{movie.Title}</h1>
                        <div className="detail--rating-labels">
                            <div>
                                <span className="relative label--border label--left label--background-yellow">
                                    <FutureImage
                                        src="/logo-imdb.svg"
                                        alt="icon-rotten-tomatoes-logo"
                                        width={32}
                                        height={24}
                                        style={{
                                            position: "absolute",
                                            top: "50%",
                                            left: "50%",
                                            transform: "translate(-50%, -50%)",
                                        }}
                                    />
                                </span>
                                <span className="label--border label--right">
                                    {movie.Ratings?.find(
                                        (r: MovieRatingProps) => r.Source === "Internet Movie Database"
                                    )?.Value ?? "N/A"}
                                </span>
                            </div>
                            <div>
                                <span className="relative label--border label--left label--background-red">
                                    <FutureImage
                                        src="/logo-rotten-tomatoes.svg"
                                        alt="icon-rotten-tomatoes-logo"
                                        width={24}
                                        height={24}
                                        style={{
                                            position: "absolute",
                                            top: "50%",
                                            left: "50%",
                                            transform: "translate(-50%, -50%)",
                                        }}
                                    />
                                </span>
                                <span className="label--border label--right">
                                    {movie.Ratings?.find((r: MovieRatingProps) => r.Source === "Rotten Tomatoes")
                                        ?.Value ?? "N/A"}
                                </span>
                            </div>
                            <div onClick={onAddToFavoritesClicked} className="cursor-pointer">
                                {!isLiked ? (
                                    <>
                                        <span className="relative label--border label--left">
                                            <FutureImage
                                                src={"/icon-heart-grey.svg"}
                                                alt="icon-heart"
                                                width={24}
                                                height={24}
                                                style={{
                                                    position: "absolute",
                                                    top: "50%",
                                                    left: "50%",
                                                    transform: "translate(-50%, -50%)",
                                                }}
                                            />
                                        </span>
                                        <span className="label--border label--right text--light-grey">
                                            Add to favourites
                                        </span>
                                    </>
                                ) : (
                                    <>
                                        <span className="relative label--border label--left label--background-red">
                                            <FutureImage
                                                src={"/icon-heart-full.svg"}
                                                alt="icon-heart"
                                                width={24}
                                                height={24}
                                                style={{
                                                    position: "absolute",
                                                    top: "50%",
                                                    left: "50%",
                                                    transform: "translate(-50%, -50%)",
                                                }}
                                            />
                                        </span>
                                        <span className="label--border label--right label--background-red">Added</span>
                                    </>
                                )}
                            </div>
                        </div>
                        <div>
                            <div className="detail--section-title">Plot</div>
                            <p className="detail--paragraph">{movie.Plot}</p>
                        </div>
                        <div className="detail-description--info-grid">
                            <div>
                                <div className="detail--section-title">Cast</div>
                                <ul>
                                    {movie.Actors?.split(", ").map((actor: string) => (
                                        <li key={actor} className="section-list">
                                            {actor}
                                        </li>
                                    ))}
                                </ul>
                            </div>
                            <div>
                                <div className="detail--section-title">Genre</div>
                                <ul>
                                    {movie.Genre?.split(", ").map((genre: string) => (
                                        <li key={genre} className="section-list">
                                            {genre}
                                        </li>
                                    ))}
                                </ul>
                            </div>
                            <div>
                                <div className="detail--section-title">Director</div>
                                <ul>
                                    {movie.Director?.split(", ").map((director: string) => (
                                        <li key={director} className="section-list">
                                            {director}
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="detail-container--poster">
                        <Image
                            loader={({ src }) => src}
                            src={movie.Poster}
                            alt={`Movie title image ${movie.Title}`}
                            className="movie-poster"
                            width={1000}
                            height={1500}
                        />
                    </div>
                </div>
            ) : isLoading ? (
                <div className="text-center">
                    <ClipLoader color={"#fff"} size={150} className="text-center" />
                </div>
            ) : isSuccess && movie && movie.Response === "False" ? (
                <div className="text-center">
                    <NotFound />
                </div>
            ) : (
                <h3>Something went terribly wrong</h3>
            )}
        </div>
    );
}
