Welcome my to the Lifebit task assignment. Below, I will show how to run the solution

## Getting Started

First, install all the required dependencies:

```bash
npm i
or
yarn
```

First, run the development server:

```bash
npm run dev
or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Also feel free to visit my "hobby" page at https://edu-t3-app.vercel.app/ for some fun projects I am trying to build like pathfinder.