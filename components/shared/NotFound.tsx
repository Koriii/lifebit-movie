import React from "react";
import Image from "next/image";

export default function NotFound() {
    return (
        <div>
            <div className="placeholder--image">
                <Image src={"/illustration-empty-state.png"} alt={`Search icon`} width={400} height={250} />
            </div>
            <div className="placeholder--title">404</div>
            <div className="placeholder--description text--light-grey">
                Unfortuantely, the movie you are looking for has not been found
            </div>
        </div>
    );
}
