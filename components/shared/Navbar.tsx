import React from "react";
import Image from "next/image";
import Link from "next/link";

export default function Navbar() {
    return (
        <div className="header cursor-pointer">
            <Link href={"/"}>
                <Image src="/logo.svg" alt="logo" width={120} height={70} />
            </Link>
        </div>
    );
}
