import React, { memo, useEffect, useRef, useState } from "react";
import Image from "next/image";
import Link from "next/link";

export interface MovieSearchProps {
    Poster: string;
    Title: string;
    Type: string;
    Year: string;
    imdbID: string;
}

interface BoxProps {
    children?: React.ReactNode;
    data: MovieSearchProps;
}

const Box = memo(({ data }: BoxProps) => {
    const [isLiked, setIsLiked] = useState(false);

    useEffect(() => {
        if (data.imdbID) {
            const storageValue = window.localStorage.getItem(data.imdbID);
            if (storageValue) {
                const parsedValue = JSON.parse(storageValue);
                setIsLiked(Boolean(parsedValue));
            }
        }
    }, []);

    const onAddToFavoritesClicked = () => {
        setIsLiked((prev) => {
            window.localStorage.setItem(data.imdbID, String(!prev));
            return !prev;
        });
    };

    return (
        <div className="box">
            <div
                className={`box--overlay-heart cursor-pointer ${isLiked ? "force-visibility" : ""}`}
                onClick={onAddToFavoritesClicked}
            >
                {!isLiked ? (
                    <Image src={"/icon-heart-grey.svg"} alt="icon-heart" width={22} height={22} />
                ) : (
                    <Image src={"/icon-heart-full.svg"} alt="icon-heart" width={22} height={22} />
                )}
            </div>
            <Link href={`/detail/${data.imdbID}`}>
                <div className="cursor-pointer">
                    <div className="box--overlay-info">
                        <div className="box--overlay-info--title">{data.Title}</div>
                        <div className="box--overlay-info--year">{data.Year}</div>
                    </div>
                    <div className="image-wrapper">
                        <Image
                            loader={({ src }) => src}
                            src={data.Poster}
                            alt={`Movie title image ${data.Title}`}
                            className="movie-poster"
                            width={800}
                            height={1200}
                        />
                    </div>
                </div>
            </Link>
        </div>
    );
});

Box.displayName = "Box";

export default Box;
